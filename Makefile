current_dir =  $(shell pwd)

postgres:
	docker run --name postgres12 -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=pass -d postgres:12-alpine

createdb:
	docker exec -it postgres12 createdb --username=root --owner=root simple_bank

migrateup:
	migrate -path db/migration -database "postgres://root:pass@localhost:5432/simple_bank?sslmode=disable" -verbose up

migratedown:
	migrate -path db/migration -database "postgres://root:pass@localhost:5432/simple_bank?sslmode=disable" -verbose down

dropdb:
	docker exec -it postgres12 drop simple_bank

sqlc:
	docker run --rm -v $(current_dir):/src -w /src sqlc/sqlc generate

test:
	go test -v -cover ./...


.PHONY: postgres createdb dropdb migrateup migratedown test