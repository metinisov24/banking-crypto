package accounts

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/metinisov24/simplebank/util"
)

func TestCreateAccount(t *testing.T) {
	arg := CreateAccountParams{
		Owner:    util.RandomOwner(),
		Balance:  util.RandomMoney(),
		Currency: util.RandomCurrency(),
	}

	account, err := testQueries.CreateAccount(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, account)

	require.Equal(t, arg.Owner, account.Owner)
	require.Equal(t, arg.Currency, account.Currency)
	require.Equal(t, arg.Balance, account.Balance)

	require.NotZero(t, account.ID)
	require.NotZero(t, account.CreatedAt)
}

func createRandomAccount(t *testing.T) Account {
	arg := CreateAccountParams{
		Owner:    util.RandomOwner(),
		Balance:  util.RandomMoney(),
		Currency: util.RandomCurrency(),
	}

	account, err := testQueries.CreateAccount(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, account)

	require.Equal(t, arg.Owner, account.Owner)
	require.Equal(t, arg.Currency, account.Currency)
	require.Equal(t, arg.Balance, account.Balance)

	require.NotZero(t, account.ID)
	require.NotZero(t, account.CreatedAt)
	return account
}
func TestGetAccount(t *testing.T) {
	account1 := createRandomAccount(t)
	account2, err := testQueries.GetAccount(context.Background(), account1.ID)

	require.NoError(t, err)
	require.NotEmpty(t, account2)

	require.Equal(t, account1.ID, account2.ID)
	require.Equal(t, account1.Owner, account2.Owner)
	require.Equal(t, account1.Balance, account2.Balance)
	require.Equal(t, account1.Currency, account2.Currency)
	require.WithinDuration(t, account1.CreatedAt, account2.CreatedAt, time.Second)

}

func TestUpdateAccount(t *testing.T) {
	account1 := createRandomAccount(t)
	uap := UpdateAccountParams{
		ID:      account1.ID,
		Balance: 314,
	}
	err := testQueries.UpdateAccount(context.Background(), uap)
	require.NoError(t, err)

	account2, err2 := testQueries.GetAccount(context.Background(), account1.ID)
	require.NoError(t, err2)

	require.Equal(t, account1.Owner, account2.Owner)
	require.Equal(t, int64(314), account2.Balance)
	require.Equal(t, account1.Currency, account2.Currency)

}

func TestRemoveAccount(t *testing.T) {
	account1 := createRandomAccount(t)
	err := testQueries.DeleteAccount(context.Background(), account1.ID)
	require.NoError(t, err)
}

func TestListAccounts(t *testing.T) {
	_ = createRandomAccount(t)
	_ = createRandomAccount(t)

	listParams := ListAccountsParams{
		Limit: 2,
	}
	accounts, err := testQueries.ListAccounts(context.Background(), listParams)
	require.NoError(t, err)
	require.GreaterOrEqual(t, len(accounts), 2)

	for _, account := range accounts {
		require.NotEmpty(t, account)
	}
}

func TestEntries(t *testing.T) {
	account1 := createRandomAccount(t)
	newEntry := AddEntryParams{
		AccountID: account1.ID,
		Amount:    300,
	}

	addedEntry, err := testQueries.AddEntry(context.Background(), newEntry)
	require.NoError(t, err)
	require.NotEmpty(t, addedEntry)

	err = testQueries.DeleteEntry(context.Background(), addedEntry.ID)
	require.NoError(t, err)

	entry, err := testQueries.GetEntry(context.Background(), addedEntry.ID)
	require.Empty(t, entry)
	require.Error(t, err)
}

func TestTransactions(t *testing.T) {
	account1 := createRandomAccount(t)
	account2 := createRandomAccount(t)

	newTransfer := AddTransferParams{
		FromAccountID: account1.ID,
		ToAccountID:   account2.ID,
		Amount:        22,
	}

	tranfer, err := testQueries.AddTransfer(context.Background(), newTransfer)
	require.NoError(t, err)
	require.NotEmpty(t, tranfer)

	err = testQueries.DeleteTransfer(context.Background(), tranfer.ID)
	require.NoError(t, err)

	transferReceived, err := testQueries.GetTransfer(context.Background(), tranfer.ID)
	require.Error(t, err)
	require.Empty(t, transferReceived)

}
