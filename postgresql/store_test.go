package accounts

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStore(t *testing.T) {
	account1 := createRandomAccount(t)
	account2 := createRandomAccount(t)

	store := NewStore(testDB)

	transferParams := TransferTxParams{
		FromAccountID: account1.ID,
		ToAccountID:   account2.ID,
		Amount:        314,
	}

	errCh := make(chan (error))
	resCh := make(chan (TransferTxResult))

	n := 10

	for i := 0; i < n; i++ {
		go func() {
			res, err := store.TransferTx(context.Background(), transferParams)
			errCh <- err
			resCh <- res
		}()
	}

	for i := 0; i < n; i++ {

		err := <-errCh
		res := <-resCh

		transfer := res.Transfer

		require.NoError(t, err)

		require.Equal(t, transfer.FromAccountID, account1.ID)
		require.Equal(t, transfer.ToAccountID, account2.ID)
		require.Equal(t, transfer.Amount, transferParams.Amount)
		require.NotZero(t, transfer.ID)
		require.NotZero(t, transfer.CreatedAt)

		_, err = store.GetTransfer(context.Background(), transfer.ID)

		require.NoError(t, err)

		fromEntry := res.FromEntry

		require.NotEmpty(t, fromEntry)
		require.NotEmpty(t, fromEntry.ID)
		require.NotEmpty(t, fromEntry.CreatedAt)
		require.Equal(t, fromEntry.Amount, -transferParams.Amount)
		require.Equal(t, fromEntry.AccountID, transfer.FromAccountID)

		_, err = store.GetEntry(context.Background(), fromEntry.ID)
		require.NoError(t, err)

		toEntry := res.ToEntry

		require.NotEmpty(t, toEntry)
		require.NotEmpty(t, toEntry.ID)
		require.NotEmpty(t, toEntry.CreatedAt)
		require.Equal(t, toEntry.Amount, transferParams.Amount)
		require.Equal(t, toEntry.AccountID, transfer.ToAccountID)

		_, err = store.GetEntry(context.Background(), toEntry.ID)
		require.NoError(t, err)

	}

}
