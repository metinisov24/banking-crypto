package accounts

import (
	"database/sql"
	"log"
	"os"
	"testing"

	_ "github.com/lib/pq"
)

var testQueries *Queries

const dbDriver = "postgres"
const dbSource = "postgres://root:pass@localhost:5432/simple_bank?sslmode=disable"

var testDB *sql.DB

func TestMain(m *testing.M) {
	var err error
	testDB, err = sql.Open(dbDriver, dbSource)
	if err != nil {
		log.Fatal("Can not connect to DB: ", err)
	}

	testQueries = New(testDB)

	os.Exit(m.Run())
}
