-- name: CreateAccount :one
INSERT INTO account (
    owner,
    balance,
    currency
) VALUES (
    $1, $2, $3
)
RETURNING *;

-- name: GetAccount :one
SELECT * FROM account
where id=$1 LIMIT 1;

-- name: ListAccounts :many
SELECT * FROM account
ORDER BY id
LIMIT $1
OFFSET $2;

-- name: UpdateAccount :exec
UPDATE account
SET balance = $2
WHERE id=$1;

-- name: DeleteAccount :exec
DELETE FROM account
WHERE id=$1;

-- name: AddEntry :one
INSERT INTO entries (
    account_id,
    amount
) VALUES (
    $1, $2
)
RETURNING *;

-- name: GetEntry :one
SELECT * FROM entries
WHERE id=$1
LIMIT 1;

-- name: DeleteEntry :exec
DELETE FROM entries
WHERE id=$1;

-- name: AddTransfer :one
INSERT INTO transfers (
    from_account_id,
    to_account_id,
    amount
) VALUES (
    $1, $2, $3
)
RETURNING *;

-- name: GetTransfer :one
SELECT * FROM transfers
WHERE id=$1
LIMIT 1;

-- name: DeleteTransfer :exec
DELETE FROM transfers
WHERE id=$1;
